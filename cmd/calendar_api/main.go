package main

import (
	"log"

	"gitlab.com/otus-go-hw/hw-26/cmd/calendar_api/api"
)

func main() {
	if err := api.RootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
