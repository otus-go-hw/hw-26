package calendar

import (
	"context"
	"time"

	"gitlab.com/otus-go-hw/hw-26/internal/domain/entities"
)

type EventAdapter interface {
	List(ctx context.Context) ([]entities.Event, error)
	Search(ctx context.Context, date time.Time) (*entities.Event, error)
	Add(ctx context.Context, event *entities.Event) error
	Update(ctx context.Context, event *entities.Event) error
	Delete(ctx context.Context, id int64) error
}
